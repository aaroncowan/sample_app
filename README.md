== README

dependencies

#Ruby and Rails versions

Ruby version 2.2.1
Rails version 4.2.0

#Other dependencies

Image Magick for photo resizing on upload

install this with:
brew install imagemagick

#Set up database with the following commands:

$ bundle exec rake db:migrate:reset
$ bundle exec rake db:seed

##to play around with this execute

$ bundle install
$ rails s

navigate to localhost:3000

#logging in:
email: example@foobar.com
password: 123456

#A note about signing up
The activation email will not be sent to the given address. 
But to get a new account working you can find a copy of the in the terminal running
the rails server. After a new account is created the activation link will be displayed. 
Copy and paste this link into your browser to activate.




